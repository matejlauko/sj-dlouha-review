<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Message
 *
 * @package App\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="message")
 */
class Message
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="applicant", type="integer")
     * @ORM\OneToOne(targetEntity="Applicant")
     * @ORM\JoinColumn(name="applicant_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    protected $applicant;

    /**
     * @ORM\Column(name="sentAt", type="datetime")
     */
    protected $sentAt;

    /**
     * @ORM\Column(name="message", type="integer")
     */
    protected $message;
}