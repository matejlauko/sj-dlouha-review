<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Applicant
 *
 * @package App\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="applicant")
 */
class Applicant
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="string")
     */
    protected $name;

    /**
     * @ORM\Column(name="surname", type="string")
     */
    protected $surname;

    /**
     * @ORM\Column(name="rating", type="integer")
     */
    protected $rating;

}