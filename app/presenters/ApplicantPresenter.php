<?php

namespace App\Presenters;

use Nette,
	App\Model;


/**
 * Applicant presenter.
 */
class ApplicantPresenter extends BasePresenter
{

	public function renderDefault()
	{
		$this->template->anyVariable = 'any value';
	}

}
